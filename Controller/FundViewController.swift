//
//  FundViewController.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/11.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

private let cellIdentifier = "LoanItemInputCell"

class FundViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var itemList = [LoanItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configLoanItem()
        self.configSubViews()
    }
    
    
    func configSubViews() {
        
        
        
        let cellNib = UINib(nibName: cellIdentifier, bundle: nil)
        self.tableView.registerNib(cellNib, forCellReuseIdentifier: cellIdentifier)
        
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.colorWith(r: 240, g: 240, b: 240)
    }
    
    
    func configLoanItem() {
        
        let totalItem = LoanItem(type: .LoanOriginal, mode: .Common)
        let termItem = LoanItem(type: .LoanTerm, mode: .Common)
        let rateItem = LoanItem(type: .LoanRate, mode: .Common)
        
        self.itemList = [totalItem,termItem,rateItem]
    }
    
    
    
}


extension FundViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.itemList.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? LoanItemInputCell
        if let itemCell = cell {
            let item = self.itemList[indexPath.row]
            itemCell.configCell(item)
            itemCell.selectionStyle = .None
            
            return itemCell
        }
        
        return UITableViewCell()
    }
    
}

extension FundViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 60.0
    }
}
