//
//  CommercialViewController.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/11.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit


private let inputCellIdentifier = "LoanItemInputCell"
private let selectCellIdentifier = "LoanItemSelectCell"
private let radioCellIdentifier = "LoanItemRadioCell"
private let presentCellIdentifier = "LoanItemPresentCell"
private let detailCellIdentifier = "PayDetailCell"

private let PAY_DATE_FORMAT = "yyyy年M月"

private let accessoryView = LoanFooterView.viewFromXib()

class CommercialViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var loanList = [LoanItem]()
    var outlineList = [LoanItem]()
    var payList = [PayItem]()
    var groupList = [[Item]]()
    
    var originalItem = LoanItem(type: .LoanOriginal, mode: .Common)
    var termItem = LoanItem(type: .LoanTerm, mode: .Common)
    var rateItem = LoanItem(type: .LoanRate, mode: .Common)
    var payDayItem = LoanItem(type: .PayDay, mode: .Common,value: NSDate().toString(PAY_DATE_FORMAT))
    var payModeItem = LoanItem(type: .PayMode, mode: .Common,value: "1")

    override func viewDidLoad() {
        super.viewDidLoad()

        self.configGroup()
        self.configSubViews()
    }
    
    
    func configSubViews() {
        
        let cellIdentifiers = [inputCellIdentifier,selectCellIdentifier,radioCellIdentifier,presentCellIdentifier,detailCellIdentifier]
        for cellIdentifier in cellIdentifiers {
            let cellNib = UINib(nibName: cellIdentifier, bundle: nil)
            self.tableView.registerNib(cellNib, forCellReuseIdentifier: cellIdentifier)
        }
        
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorColor = UIColor.colorWith(r: 240, g: 240, b: 240)
    }

   
    func configGroup() {
        
        self.groupList.removeAll()
        
        self.loanList = [originalItem,termItem,rateItem,payDayItem,payModeItem]
        self.groupList.append(self.loanList)
        
        if self.outlineList.count > 0 {
            self.groupList.append(self.outlineList)
        }
        
        if self.payList.count > 0 {
            self.groupList.append(self.payList)
        }
    }
    
    func presendDataPicker(item: LoanItem) {
        
        let datePicker = DatePickerViewController()
        datePicker.view.backgroundColor = UIColor.clearColor()
        datePicker.selectedDate = item.value?.toDate(PAY_DATE_FORMAT)
        datePicker.completionHandler = {(date) in
            datePicker.dismissViewControllerAnimated(true, completion: nil)
            item.value = date.toString(PAY_DATE_FORMAT)
            self.tableView.reloadData()
            
        }
        
        let os = NSProcessInfo().operatingSystemVersion
        if os.majorVersion >= 8 {
            datePicker.modalPresentationStyle = .OverCurrentContext
        }
        else{
            self.modalPresentationStyle = .CurrentContext
        }
        
        self.presentViewController(datePicker, animated: true, completion: nil)
    }

    
    func validateInputs() -> Bool {
        
        var invalidItem:LoanItem? = nil
        for item in loanList {
            if item.value == nil || item.value!.characters.count == 0 {
                invalidItem = item
                break
            }
        }
        
        if let item = invalidItem {
            let message = "\(item.type.title())不能为空"
            AlertManager.alert(nil, message: message, actions: ["确定"], completionHandler: { (selectedIndex) in
                
            })
            return false
        }
        else{
            return true
        }
    }
    
    func calculateLoanDetail() {
        
        let original = Double(self.originalItem.value ?? "") ?? 0
        let rate = Double(self.rateItem.value ?? "") ?? 0
        let term = Double(self.termItem.value ?? "") ?? 0
        let interest = original*rate*term/100.0
        let total = original + interest
        let monthly = total/(term*12)
        
        let totalItem = LoanItem(type: .PayTotal, mode: .Common, value: "\(original+interest) 万元")
        let interestItem = LoanItem(type: .PayInterest, mode: .Common, value: "\(interest) 万元")
        let monthlyItem = LoanItem(type: .PayMonthly, mode: .Common, value: (monthly*10000).format(".3")+" 元")
        
        self.outlineList = [totalItem,interestItem,monthlyItem]
        
    }

}


extension CommercialViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.groupList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.groupList[section].count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let item = self.groupList[indexPath.section][indexPath.row]
    
        let cellIdentifier = item.type.cellIdentifier()
        let cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? LoanItemCell
        if let itemCell = cell {
            
            itemCell.configCell(item)
            itemCell.selectionStyle = .None
            itemCell.actionFinishHandler = { (value) in
                
                if let loanItem = item as? LoanItem {
                    loanItem.value = value
                    if loanItem.type == .PayDay {
                        self.presendDataPicker(loanItem)
                    }
                }
                
            }
            
            if let inputCell = (itemCell as? LoanItemInputCell) {
                inputCell.inputTF.inputAccessoryView = accessoryView
            }
            
            return itemCell
        }
        
        
        return UITableViewCell()
    }
}

extension CommercialViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 50.0
    }
    
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        
//        if section == 0 {
//            return 50.0
//        }
//        else{
//            return 0.1
//        }
//    }
//    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        
//        if section == 0 {
//            let footerView = LoanFooterView.viewFromXib() as? LoanFooterView
//            if let footer = footerView {
//                footer.resetActionHandler = {
//                    
//                }
//                footer.calculateActionHandler = {
//                    
//                    if self.validateInputs() {
//                        self.calculateLoanDetail()
//                        self.configGroup()
//                        self.tableView.reloadData()
//                    }
//                }
//            }
//            
//            return footerView
//        }
//        else {
//            return nil
//        }
//    }
}
