//
//  DatePickerViewController.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/24.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    @IBOutlet var datePicker: UIDatePicker!
    
    var selectedDate: NSDate?
    var completionHandler:((date: NSDate) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTapClicked)))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.datePicker.date = self.selectedDate ?? NSDate()
    }
    
    func onTapClicked(sender: AnyObject) {
        
        self.completionHandler?(date: self.datePicker.date)
    }


}
