//
//  AppDelegate.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/11.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        self.configView()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func configView() {
        
        UITabBar.appearance().tintColor = UIColor.colorWith(r: 85, g: 185, b: 190)
        
        let commercial = CommercialViewController()
        commercial.tabBarItem.title = "商业贷款"
        commercial.tabBarItem.image = UIImage(named: "commercial")
        commercial.tabBarItem.selectedImage = commercial.tabBarItem.selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        let fund = FundViewController()
        fund.tabBarItem.title = "公积金"
        fund.tabBarItem.image = UIImage(named: "fund")
        fund.tabBarItem.selectedImage = fund.tabBarItem.selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        let group = GroupViewController()
        group.tabBarItem.title = "组合贷款"
        group.tabBarItem.image = UIImage(named: "group")
        group.tabBarItem.selectedImage = group.tabBarItem.selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        let compare = CommercialViewController()
        compare.tabBarItem.title = "比较"
        compare.tabBarItem.image = UIImage(named: "compare")
        compare.tabBarItem.selectedImage = compare.tabBarItem.selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        let tabbar = UITabBarController()
        tabbar.setViewControllers([commercial,fund,group,compare], animated: false)
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = tabbar
        self.window?.makeKeyAndVisible()
    }

}

