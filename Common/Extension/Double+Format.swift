//
//  Double+Format.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/26.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation

extension Double {
    
    func format(digit: String) -> String {
        
        return String(format: "%\(digit)f", self)
    }
    
}