//
//  UIView+Xib.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class func viewFromXib() -> UIView? {
        
        let result: UIView?
        
        let className = NSStringFromClass(self).componentsSeparatedByString(".").last!
        
        result = UINib(
            nibName: className,
            bundle: NSBundle.mainBundle()
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
        
        
        return result
    }
    
    
}

