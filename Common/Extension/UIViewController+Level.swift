//
//  UIViewController+Level.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    class func topViewController(viewController: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
        
    }
}