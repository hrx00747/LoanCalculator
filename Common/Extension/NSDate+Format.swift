//
//  NSDate+Format.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation

extension NSDate {
    
    func toString(format: String) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.stringFromDate(self)
    }
    
}
