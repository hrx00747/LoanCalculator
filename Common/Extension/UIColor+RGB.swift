//
//  UIColor+RGB.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/13.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func colorWith(r r: Int, g: Int, b: Int) -> UIColor{
        
        return UIColor(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1.0)
    }
    
}
