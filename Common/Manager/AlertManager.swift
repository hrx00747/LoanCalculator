//
//  AlertManager.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation
import UIKit

class AlertManager {
    
    class func alert(title:String?, message:String?, actions:[String], completionHandler: ((selectedIndex:NSInteger) -> Void)){
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        for index in 0 ..< actions.count {
            let action  = UIAlertAction(title: actions[index], style: .Default, handler: { (actions) in
                completionHandler(selectedIndex: index)
            })
            alertController.addAction(action)
        }
        
        let topController = UIViewController.topViewController()
        topController?.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
}

