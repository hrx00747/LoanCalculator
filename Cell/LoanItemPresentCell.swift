//
//  LoanItemPresentCell.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/13.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class LoanItemPresentCell: LoanItemCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func configCell(item: Item) {
        
        if let loanItem = item as? LoanItem {
            self.titleLabel.text = loanItem.mode.title() + loanItem.type.title()
            self.valueLabel.text = String(loanItem.value!)
        }
    }
    
}
