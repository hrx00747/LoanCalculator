//
//  LoanItemSelectCell.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/13.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class LoanItemSelectCell: LoanItemCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectButton.layer.cornerRadius = 3.0
        self.selectButton.layer.masksToBounds = true
        self.selectButton.layer.borderWidth = 0.5
        self.selectButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.selectButton.addTarget(self, action: #selector(onButtonClicked), forControlEvents: .TouchUpInside)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func configCell(item: Item) {
        
        if let loanItem = item as? LoanItem {
            self.titleLabel.text = "\(loanItem.mode.title())\(loanItem.type.title())"
            self.selectButton.setTitle(loanItem.value, forState: .Normal)
        }
    }
    
    func onButtonClicked(sender: AnyObject) {
        
        if let dateText = self.selectButton.titleForState(.Normal) {
            self.actionFinishHandler?(value: dateText)
        }
        
    }
    
}
