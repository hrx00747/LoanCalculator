//
//  LoanItemCell.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/11.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class LoanItemInputCell: LoanItemCell,UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.inputTF.delegate = self
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func configCell(item: Item) {
        
        if let loanItem = item as? LoanItem {
            self.titleLabel.text = "\(loanItem.mode.title())\(loanItem.type.title())"
            self.inputTF.placeholder = "\(loanItem.type.hint())"
        }
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        self.actionFinishHandler?(value: textField.text ?? "")
    }
    
}
