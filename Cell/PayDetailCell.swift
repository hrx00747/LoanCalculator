//
//  PayDetailCell.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

private let PAY_DATE_FORMAT = "yyyy.M"

class PayDetailCell: LoanItemCell {
    
    @IBOutlet weak var sequenceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func configCell(item: Item) {
        
        if let payItem = item as? PayItem {
            self.sequenceLabel.text = "\(payItem.sequence)"
            let payDate = NSDate(timeIntervalSince1970: payItem.payDate)
            self.dateLabel.text = "\(payDate.toString(PAY_DATE_FORMAT))"
            
            let nf = NSNumberFormatter()
            nf.numberStyle = NSNumberFormatterStyle.DecimalStyle
            nf.maximumFractionDigits = 2
            self.detailLabel.text = "\(nf.stringFromNumber(payItem.original)) / \(nf.stringFromNumber(payItem.interest))"
        }
    }
    
}
