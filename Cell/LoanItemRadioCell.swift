//
//  LoanItemRadioCell.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/13.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class LoanItemRadioCell: LoanItemCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueSegment: UISegmentedControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.valueSegment.tintColor = UIColor.colorWith(r: 85, g: 185, b: 190)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func configCell(item: Item) {
        
        if let loanItem = item as? LoanItem {
            self.titleLabel.text = "\(loanItem.mode.title())\(loanItem.type.title())"
            let value = loanItem.value ?? "0"
            self.valueSegment.selectedSegmentIndex = Int(value) ?? 0
                
        }
        
    }
    
}
