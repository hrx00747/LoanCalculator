//
//  LoanFooterView.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import UIKit

class LoanFooterView: UIView {

    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var calculateButton: UIButton!
    
    var resetActionHandler: dispatch_block_t?
    var calculateActionHandler: dispatch_block_t?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.resetButton.layer.cornerRadius = 3.0
        self.resetButton.layer.masksToBounds = true;
        self.resetButton.layer.borderWidth = 0.5
        self.resetButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        
        self.calculateButton.layer.cornerRadius = 3.0
        self.calculateButton.layer.masksToBounds = true;
        self.calculateButton.layer.borderWidth = 0.5
        self.calculateButton.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    
    @IBAction func onSaveClicked(sender: AnyObject) {
        
        self.resetActionHandler?()
    }
    
    @IBAction func onCalculateClicked(sender: AnyObject) {
        
        self.calculateActionHandler?()
    }

}
