//
//  Item.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation

enum LoanItemType {
    
    case LoanOriginal       //贷款金额
    case LoanRate           //贷款利率
    case LoanTerm           //贷款期限
    case Discount           //折扣
    case PayDay             //还款日期
    case PayMode            //还款方式
    
    case PayInterest        //利息总额
    case PayTotal           //还款总额
    case PayMonthly         //月还款
    
    case PayDetail          //每月还款详情
    
    
    func title() -> String {
        
        switch self {
        case .LoanOriginal:
            return "贷款总额:"
        case .LoanRate:
            return "贷款利率:"
        case .LoanTerm:
            return "贷款期限:"
        case .Discount:
            return "折扣:"
        case .PayDay:
            return "还款日期:"
        case .PayMode:
            return "还款方式:"
        case .PayInterest:
            return "利息总额:"
        case .PayTotal:
            return "还款总额:"
        case .PayMonthly:
            return "月还款:"
        case .PayDetail:
            return ""
        }
    }
    
    func hint() -> String {
        
        switch self {
        case .LoanOriginal:
            return "万元"
        case .LoanRate:
            return "%"
        case .LoanTerm:
            return "年"
        case .Discount:
            return "1.0"
        default:
            return ""
        }
    }
    
    func cellIdentifier() -> String {
        
        switch self {
        case .LoanOriginal,.LoanTerm,.LoanRate,.Discount:
            return "LoanItemInputCell"
        case .PayDay:
            return "LoanItemSelectCell"
        case .PayMode:
            return "LoanItemRadioCell"
        case .PayInterest,.PayTotal,.PayMonthly:
            return "LoanItemPresentCell"
        case .PayDetail:
            return "PayDetailCell"
        }
    }
}

enum LoanMode {
    
    case Common
    case Commercial
    case Fund
    
    func title() -> String {
        
        switch self {
        case .Common:
            return ""
        case .Commercial:
            return "商业"
        case .Fund:
            return "公积金"
        }
    }
    
}

class Item {
    
    var type: LoanItemType = .LoanOriginal
    var mode: LoanMode = .Commercial
    
    init(type: LoanItemType, mode: LoanMode) {
        self.type = type
        self.mode = mode
    }
    
}