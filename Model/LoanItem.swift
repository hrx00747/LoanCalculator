//
//  LoanItem.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/11.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation

class LoanItem : Item {
    
    var value: String?
    
    
    override init(type:LoanItemType, mode: LoanMode) {
        
        super.init(type: type, mode: mode)

    }
    
    init(type:LoanItemType, mode:LoanMode, value:String) {
        
        super.init(type: type, mode: mode)
        self.value = value
    }
    
}
