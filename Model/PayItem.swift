//
//  PayItem.swift
//  LoanCalculator
//
//  Created by Chris Huang on 16/9/25.
//  Copyright © 2016年 group4app. All rights reserved.
//

import Foundation


class PayItem : Item{
    
    var sequence: Int = 0
    var payDate: Double = 0.0
    var total: Double = 0.0
    var original: Double = 0.0
    var interest: Double = 0.0
    
    
}